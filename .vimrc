call plug#begin()

Plug 'itchyny/lightline.vim' "Highlights lines
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim' "Fuzzy find plugin
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim' "The One Dark Theme
Plug 'jiangmiao/auto-pairs'
Plug 'preservim/nerdtree'
Plug 'junegunn/goyo.vim' "Removes Line numbers for focusing
Plug 'godlygeek/tabular' "Auto formatting
Plug 'plasticboy/vim-markdown' "Markdown syntax highlighting
Plug 'ryanoasis/vim-devicons' "Cool icons for nerd tree
Plug 'Xuyuanp/nerdtree-git-plugin' "nerd tree customization
Plug '907th/vim-auto-save' "auto saves files as you edit

call plug#end()

" Startup Settings
    syntax on
  let mapleader=" " "Maps Leader to space
  let NERDTreeShowHidden=1
  set number relativenumber
  set nowrap
  set noshowmode
  set laststatus=2
  set mouse=a "Allows mouse usage inside vim. Great for noobs.
  set clipboard=unnamedplus "Remaps default copy paste to system clipboard
  colorscheme gruvbox
  set background=dark
  set incsearch
  set history=5000
  set tabstop=4
  set softtabstop=4
  let g:auto_save = 1
  let g:auto_save_events = ["InsertLeave", "TextChanged"]
  set cursorline
  highlight CursorLine ctermbg=Yellow cterm=bold guibg=#2b2b2b

" Tabedit keybinds
  nnoremap <Leader>1 1gt<CR>
  nnoremap <Leader>2 2gt<CR>
  nnoremap <Leader>3 3gt<CR>
  nnoremap <Leader>4 4gt<CR>
  nnoremap <Leader>5 5gt<CR>
  nnoremap <Leader>t :tabnew<CR>
  nnoremap <Leader>c :tabclose<CR>

" Plugin Shortcuts
  map ; :Files<CR>
  nnoremap <leader>n :NERDTreeFocus<CR>
  nnoremap <C-n> :NERDTree<CR>
  nnoremap <C-t> :NERDTreeToggle<CR>
  nnoremap <C-f> :NERDTreeFind<CR>

" General Shortcuts
  nnoremap S :%s//g<Left><Left> 
  nmap <Leader>r :w<CR>:so %<CR>
  nmap <Leader>s :w<CR>
  map <Leader>e $
